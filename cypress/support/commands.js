import "cypress-localstorage-commands";

Cypress.Commands.add('loginAs', (username, password) => {
      cy.visit('/')
      cy.get('#signInName').clear().click().type(username)
      cy.get('#continue').click()
      cy.get('#password').type(password)
      cy.get('#next').click()
      cy.wait(5000)
      localStorage.setItem('test_storage','available');
      cy.saveLocalStorage()
  })

  Cypress.Commands.add('postToken', () => {
    cy.request({
        method: 'POST',
        url: `https://scaleaqcustomerdev.b2clogin.com/scaleaqcustomerdev.onmicrosoft.com/B2C_1_password_credentials/oauth2/v2.0/token`,
        qs: {
          grant_type: 'password',
          client_id: '9ca2963d-2d62-47c8-85dd-b183c49bb444',
          scope: 'openid 9ca2963d-2d62-47c8-85dd-b183c49bb444',
          response_type: 'id_token',
          username: "scaleaq.admin@scaleaq.com",
          password: "*5Na7SZHy!HR9C"
        },
      }).then(({ body }) => {
        const { id_token } = body
        cy.setLocalStorage("identity_token", body.id_token);
        cy.log(id_token)
      })
  });

  Cypress.Commands.add('login', (username, password) => {
    cy.session([username, password], () => {
      cy.visit('/')
      cy.get('#signInName').clear().click().type(username)
      cy.get('#continue').click()
      cy.get('#password').type(password)
      cy.get('#next').click()
      cy.wait(5000)
    })
  })

  //cy.setLocalStorage("identity_token", body);