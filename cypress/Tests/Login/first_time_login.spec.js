/// <reference types="cypress" />

describe('Login for the first time', () => {

    context('Management Portal', function () {

        it('Open Management Portal ', () => {
            cy.visit('https://managementportal.scaleaq-dev.net/')
            cy.focused().should('have.attr', 'title').and('eq', 'Email address to use for signing in.')
            cy.get('#welcome-message').contains("Continue to Management Portal (dev)")   
        })

        it('Use invalid email address with ENTER key press ', () => {
            cy.visit('https://managementportal.scaleaq-dev.net/')
            cy.get('#signInName').type("wrong_email.com").type('{enter}')
            cy.get('.attrEntry > .error').contains("Email address is not valid.")
        })

        it('Use invalid email address with NEXT button ', () => {
            cy.visit('https://managementportal.scaleaq-dev.net/')
            cy.get('#signInName').type("wrong_email.com")
            cy.get('#continue').click()
            cy.get('.attrEntry > .error').contains("Email address is not valid.")
        })
    })

    context('Knowledger', function () {      

        it('Open Knowledger ', () => {
            cy.visit('https://knowledger.scaleaq-dev.net/')
            cy.focused().should('have.attr', 'title').and('eq', 'Email address to use for signing in.')
            cy.get('#welcome-message').contains("Continue to Knowledger (dev)")   
        })

        it('Use invalid email address with ENTER key press ', () => {
            cy.visit('https://knowledger.scaleaq-dev.net/')
            cy.get('#signInName').type("wrong_email.com").type('{enter}')
            cy.get('.attrEntry > .error').contains("Email address is not valid.")
        })

        it('Use invalid email address with NEXT button ', () => {
            cy.visit('https://knowledger.scaleaq-dev.net/')
            cy.get('#signInName').type("wrong_email.com")
            cy.get('#continue').click()
            cy.get('.attrEntry > .error').contains("Email address is not valid.")
        })
    })

    context('Feed Manager', function () {      

        it('Open Feed Manager ', () => {
            cy.visit('https://feedmanager.scaleaq-dev.net/')
            cy.focused().should('have.attr', 'title').and('eq', 'Email address to use for signing in.')
            cy.get('#welcome-message').contains("Continue to Feed Manager (dev)") 
        })

        it('Use invalid email address with ENTER key press ', () => {
            cy.visit('https://feedmanager.scaleaq-dev.net/')
            cy.get('#signInName').type("wrong_email.com").type('{enter}')
            cy.get('.attrEntry > .error').contains("Email address is not valid.")
        })

        it('Use invalid email address with NEXT button ', () => {
            cy.visit('https://feedmanager.scaleaq-dev.net/')
            cy.get('#signInName').type("wrong_email.com")
            cy.get('#continue').click()
            cy.get('.attrEntry > .error').contains("Email address is not valid.")
        })
    })
        /////////Add More Tests
  })
  