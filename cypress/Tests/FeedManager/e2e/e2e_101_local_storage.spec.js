/// <reference types="cypress" />

describe('example e2e tests', () => {
  before(() => {
    cy.loginAs("testuser1@qa.com","scaleAQ..") 
    cy.restoreLocalStorage()
  })

  beforeEach(() => {
   cy.restoreLocalStorage();
  })
  it('test 1', () => {
    
    cy.visit('/')
    cy.getLocalStorage("test_storage").should("equal", "available");
  })

  it('test 2', () => {
    cy.visit('/')
    cy.getLocalStorage("test_storage").should("equal", "available");
    cy.contains('Hjem')
    cy.contains('Fôrtyper')
    cy.contains('Fôrlagre')
  
  })

  it("test 3", () => {
    cy.visit('/')
    cy.getLocalStorage("test_storage").should("equal", "available");
  });
 
})
