/// <reference types="cypress" />

describe('example api tests', () => {
  before(() => {
    cy.postToken();
    cy.saveLocalStorage();
  })

  beforeEach(() => {
    cy.restoreLocalStorage();
  })
  it('test 1', () => {
    cy.getLocalStorage("identity_token").should("exist");
  
  })

  it("should exist identity in localStorage", () => {
    cy.getLocalStorage("identity_token").should("exist");
    cy.getLocalStorage("identity_token").then(token => {
      console.log("Identity token", token);
    });
  });

  it.only('get user by id', () => {
    cy.request({
        method : 'GET',
        url : `https://feedmanager.api.scaleaq-dev.net/Transaction`,
        qs: {
          FromTime: '2021-12-02T00:00:10.714Z',
          SiloIds: '870',
        },
        headers: {
            'authorization': 'Bearer '+localStorage.getItem('identity_token'),
          }
    }).then((res)=>{
        expect(res.status).to.eq(200)
        expect(res.body[0].siloId).to.eq(870)
    })    
})  
 
})
